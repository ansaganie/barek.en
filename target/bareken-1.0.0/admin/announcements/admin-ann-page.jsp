<%@ page import="com.epam.ans.app.entities.announcement.Announcement" %>
<%@ page import="com.epam.ans.app.entities.person.Person" %>
<%@ page import="com.epam.ans.app.entities.announcement.Contact" %>
<%@ page import="com.epam.ans.app.model.DAO" %><%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 03.03.21
  Time: 17:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%
    Announcement announcement = (Announcement) request.getAttribute("announcement-show");
    long id = announcement.getId();
    String title = announcement.getTitle();
    String createDate = announcement.getCreateDate().toString();
    String content = announcement.getContent();
    long ownerId = announcement.getAnnouncerId();
    Person owner = DAO.getUserDAO().getUserById(ownerId);
    String ownerName = owner.getUserName();
    Contact contact = announcement.getContact();
    String email = contact.getEmail();
    String phoneNumber = contact.getPhoneNumber();
    String location = contact.getLocation();
    double value = announcement.getValue();
    String category = announcement.getCategory().name();
    String typeOfAnnouncement = announcement.getTypeOfAnnouncement().name();
    %>
<html>
<head>
    <title>Bareken: <%=title%> </title>
    <%@include file="../header.jsp"%>

    <div class="container m-5">
        <div class="card text-dark bg-light mb-3" style="max-width: 50rem;">
            <div class="card-body">
                <p class="card-text"><%=title%></p>
            </div>
            <div class="card-header">Type of announcement</div>
            <div class="card-body">
                <p class="card-text"><%=typeOfAnnouncement%></p>
            </div>
            <div class="card-body">
                <p class="card-text"><%=category%></p>
            </div>
            <div class="card-body">
                <p class="card-text"><%=createDate%></p>
            </div>
            <div class="card-body">
                <p class="card-text"><%=content%></p>
            </div>
            <div class="card-body">
                <p class="card-text"><%=value%> KZT</p>
            </div>
            <div class="card-header">Author of advertisement</div>
            <div class="card-body">
                <p class="card-text"><%=ownerName%></p>
            </div>
            <div class="card-header">Contact information</div>
            <div class="card-header">Contacts</div>
            <div class="card-body">
                <p class="card-text"><%=email%></p>
            </div>
            <div class="card-body">
                <p class="card-text"><%=phoneNumber%></p>
            </div>
            <div class="card-header">Location</div>
            <div class="card-body">
                <p class="card-text"><%=location%></p>
            </div>
        </div>
        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
            <a type="button" class="btn btn-success" href="/admin/announcements/announcement?id=<%=id%>&action=APPROVE">APPROVE</a>
            <a type="button" class="btn btn-warning" href="/admin/announcements/announcement?id=<%=id%>&action=REJECT">REJECT</a>
            <a type="button" class="btn btn-danger" href="/admin/announcements/announcement?id=<%=id%>&action=DELETE">DELETE</a>
        </div>
    </div>


    <%@include file="../footer.jsp"%>
