<%@ page import="com.epam.ans.app.entities.announcement.Announcement" %>
<%@ page import="com.epam.ans.app.model.DAO" %>
<%@ page import="java.rmi.ServerException" %>
<%@ page import="com.epam.ans.app.entities.announcement.Contact" %>
<%@ page import="com.epam.ans.app.entities.person.Person" %>

<%@ page contentType="text/html;charset=UTF-8"%>
        <%Announcement announcement;
        String id  = request.getParameter("id");
        String title = null;
        String create_date = null;
        String content = null;
        long ownerId;
        Person owner;
        String ownerName = null;
        Contact contact;
        String email = null;
        String phoneNumber = null;
        String location = null;
        double value = 0;
        String category = null;
        String typeOfAnnouncement = null;
        long announcementId;
        if (id != null) {
            try {
                announcementId  = Long.parseLong(id);
            } catch (NumberFormatException e) {
                throw new ServerException("There is no such announcement with id = " + id);
            }
            announcement = DAO.getAnnouncementDAO().getAnnouncementById(announcementId);
            if (announcement != null) {
                title = announcement.getTitle();
                create_date = announcement.getCreateDate().toString();
                content = announcement.getContent();
                ownerId = announcement.getAnnouncerId();
                owner = DAO.getUserDAO().getUserById(ownerId);
                ownerName = owner.getUserName();
                contact = announcement.getContact();
                email = contact.getEmail();
                phoneNumber = contact.getPhoneNumber();
                location = contact.getLocation();
                value = announcement.getValue();
                category = announcement.getCategory().name();
                typeOfAnnouncement = announcement.getTypeOfAnnouncement().name();
            } else throw new ServerException("There is no such announcement with id = " + announcementId);

        } else {
            response.sendRedirect("/announcements");
        }%>
<html>
<head>
    <title>Bareken: <%=title%> </title>
    <%@include file="/header.jsp"%>

    <div class="container m-5">
        <div class="card text-dark bg-light mb-3" style="max-width: 50rem;">
            <div class="card-body">
                <p class="card-text"><%=title%></p>
            </div>
            <div class="card-header">Type of announcement</div>
            <div class="card-body">
                <p class="card-text"><%=typeOfAnnouncement%></p>
            </div>
            <div class="card-body">
                <p class="card-text"><%=category%></p>
            </div>
            <div class="card-body">
                <p class="card-text"><%=create_date%></p>
            </div>
            <div class="card-body">
                <p class="card-text"><%=content%></p>
            </div>
            <div class="card-body">
                <p class="card-text"><%=value%> KZT</p>
            </div>
            <div class="card-header">Author of advertisement</div>
            <div class="card-body">
                <p class="card-text"><%=ownerName%></p>
            </div>
            <div class="card-header">Contact information</div>
            <div class="card-header">Contacts</div>
            <div class="card-body">
                <p class="card-text"><%=email%></p>
            </div>
            <div class="card-body">
                <p class="card-text"><%=phoneNumber%></p>
            </div>
            <div class="card-header">Location</div>
            <div class="card-body">
                <p class="card-text"><%=location%></p>
            </div>

        </div>
    </div>


    <%@include file="/footer.jsp"%>

