<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
  <title>Bareken: Activate announcement</title>
  <%@include file="/header.jsp"%>
  <div class="container">
    <form class="row g-3" method="post">
      <h1 class="h3 m-5 text-center font-weight-normal">Do you really want to activate your announcement?</h1>
      <div class="form-group row" role="group">
        <div class="btn-group row col-md-12">
          <div class="col-md-5"></div>
          <div class="col-md-1 form-check form-check-inline">
            <input type="submit" class="btn-check" name="answer-activate-announcement" id="yes" value="YES">
            <label class="btn btn-outline-danger" for="yes">YES</label>
          </div>
          <div class="col-md-1 form-check form-check-inline">
            <input type="submit" class="btn-check" name="answer-activate-announcement" id="no" value="NO" checked>
            <label class="btn btn-outline-success" for="no">NO</label>
          </div>
          <div class="col-md-5"></div>
        </div>
      </div>
    </form>

  </div>
  <%@include file="/footer.jsp"%>
