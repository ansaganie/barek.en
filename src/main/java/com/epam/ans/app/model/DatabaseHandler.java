package com.epam.ans.app.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseHandler {
    private static DatabaseHandler dbHandler;

    public Connection getDbConnection() throws SQLException {
        String dbHost = "localhost";
        String dbPort = "3306";
        String dbName = "bareken_db";
        String connectionString = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName;

        // need to read what is Class.forName for

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException exception) {
            exception.printStackTrace();
        }

        String dbUser = "root";
        String dbPass = "Frnj,t2020";
        return DriverManager.getConnection(connectionString, dbUser, dbPass);
    }

    private DatabaseHandler(){}

    public static DatabaseHandler getInstance() {
        if (dbHandler == null) {
            dbHandler = new DatabaseHandler();
        }
        return dbHandler;
    }

    public void printSQLException(SQLException exception) {
        for (Throwable ex : exception) {
            if (ex instanceof SQLException) {
                ex.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) ex).getSQLState());
                System.err.println("ErrorCode: " + ((SQLException) ex).getErrorCode());
                System.err.println("Message: " + ex.getMessage());
                Throwable causeException = ex.getCause();
                while (causeException !=null) {
                    System.out.println("Cause: " + causeException);
                    causeException = causeException.getCause();
                }
            }
        }
    }
}
