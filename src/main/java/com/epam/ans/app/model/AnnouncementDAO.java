package com.epam.ans.app.model;

import com.epam.ans.app.entities.announcement.*;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Set;
import java.util.TreeSet;
import java.util.Comparator;

public class AnnouncementDAO {
    private final String AD_TABLE_NAME = "announcement";
    private final String AD_ID = "id";
    private final String AD_TITLE = "title";
    private final String AD_CONTENT = "content";
    private final String AD_OWNER = "owner";
    private final String AD_VALUE = "value";
    private final String AD_CATEGORY = "category";
    private final String AD_TYPE = "type_of_advert";
    private final String AD_STATUS = "status";

    private final DatabaseHandler dbHandler = DatabaseHandler.getInstance();

    //create
    public boolean createAnnouncement(Announcement announcement) {
        boolean createAnn = false;
        boolean createContact = false;
        String statement = String.format("insert into " + AD_TABLE_NAME + " ( %s, %s, %s, %s, %s, %s, %s, %s) " +
                        "values (?, ?, ?, ?, ?, ?, ?, ?);", AD_ID, AD_TITLE, AD_CONTENT,
                AD_OWNER, AD_VALUE, AD_CATEGORY, AD_TYPE, AD_STATUS);
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = getStatement(statement, announcement, connection)) {
            System.out.println("createAnnouncement" + ps);
            createAnn = ps.executeUpdate() > 0;

            createContact = DAO.getContactDAO().insertContact(announcement.getContact());
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return createAnn && createContact;
    }

    //read
    public Announcement getAnnouncementById(Long id) {
        Announcement announcement = null;

        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + AD_TABLE_NAME +
                     " WHERE " + AD_ID + " = " + id + ";")) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                announcement = createAnnouncement(resultSet);
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }

        return announcement;
    }

    public Set<Announcement> getAnnouncementsByUserId(Long userId) {
        Set<Announcement> announcements = new TreeSet<>(Comparator.comparing(Announcement::getCreateDate));
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + AD_TABLE_NAME + " WHERE " +
                     AD_OWNER + " = " + userId + ";")) {
            System.out.println(ps);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                announcements.add(createAnnouncement(resultSet));
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return announcements;
    }

    public Set<Long> getAllAnnouncementsId() {
        Set<Long> ids = new TreeSet<>();
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select " + AD_ID + " from " + AD_TABLE_NAME + ";")) {
            ResultSet resultSet = ps.executeQuery();
            System.out.println("get all announcements id " + ps);
            while (resultSet.next()) {
                ids.add(resultSet.getLong(AD_ID));
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }

        return ids;
    }

    public Set<Announcement> getAnnouncements() {
        Set<Announcement> announcements = new TreeSet<>(Comparator.comparing(Announcement::getCreateDate));
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select * from " + AD_TABLE_NAME + ";")) {
            ResultSet resultSet = ps.executeQuery();
            System.out.println("getAllAnnouncements " + ps);
            while (resultSet.next()) {
                Announcement announcement = createAnnouncement(resultSet);
                announcements.add(announcement);
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return announcements;
    }

    public Set<Announcement> getAnnouncements(AnnouncementStatus announcementStatus) {
        Set<Announcement> announcements = new TreeSet<>(Comparator.comparing(Announcement::getCreateDate));
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select * from " + AD_TABLE_NAME + " where " +
                     AD_STATUS + " = ?;")) {
            ps.setString(1, announcementStatus.name() );
            System.out.println("getAllActiveAnnouncements " + ps);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Announcement announcement = createAnnouncement(resultSet);
                announcements.add(announcement);
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return announcements;
    }

    public Set<Announcement> getAnnouncements(AnnouncementStatus announcementStatus, TypeOfAnnouncement typeOfAnnouncement) {
        Set<Announcement> announcements = new TreeSet<>(Comparator.comparing(Announcement::getCreateDate));
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select * from " + AD_TABLE_NAME + " where " +
                     AD_STATUS + " = ? and " + AD_TYPE + " = ?;")) {
            ps.setString(1, announcementStatus.name());
            ps.setString(2, typeOfAnnouncement.name());
            System.out.println("getAllActiveAnnouncements " + ps);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Announcement announcement = createAnnouncement(resultSet);
                announcements.add(announcement);
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return announcements;
    }

    public Long getAnnouncementsCount() {
        return DAO.countRows(AD_TABLE_NAME);
    }

    public Long getAnnouncementsCount(TypeOfAnnouncement typeOfAnnouncement) {
        return DAO.countRows(AD_TABLE_NAME, AD_TYPE, typeOfAnnouncement.name());
    }

    //update
    public boolean updateAnnouncement(Announcement announcement) {
        boolean annUpdated = false;
        boolean contactUpdated = false;
        String statement = String.format("update %s set %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?" +
                        " where %s = ?;", AD_TABLE_NAME, AD_TITLE, AD_CONTENT, AD_VALUE, AD_CATEGORY, AD_TYPE,
                AD_STATUS, AD_ID);
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = getUpdateStatement(statement, announcement, connection)){
            System.out.println("updateAnnouncement " + ps);
            annUpdated = ps.executeUpdate() > 0;
            contactUpdated = DAO.getContactDAO().updateContact(announcement.getContact());
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return annUpdated && contactUpdated;
    }

    public boolean changeStatus(Long id, AnnouncementStatus announcementStatus) {
        boolean changed = false;
        String statement = String.format("update %s set %s = ?" +
                " where %s = ?;", AD_TABLE_NAME, AD_STATUS, AD_ID);
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement(statement)){
            ps.setString(1, announcementStatus.name());
            ps.setLong(2, id);
            changed = ps.executeUpdate() > 0;
            System.out.println("changeStatusAnnouncement " + ps);
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return changed;
    }

    //delete
    public boolean deleteAnnouncementById(long id) {
        return DAO.deleteRowById(AD_TABLE_NAME, AD_ID, id);
    }

    private Announcement createAnnouncement(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong(AD_ID);
        String title = resultSet.getString(AD_TITLE);
        String AD_TIMESTAMP = "create_date";
        Timestamp create_date = resultSet.getTimestamp(AD_TIMESTAMP);
        String content = resultSet.getString(AD_CONTENT);
        long owner = resultSet.getLong(AD_OWNER);
        Contact contact = DAO.getContactDAO().getContactByOwnerId(id);
        double value = resultSet.getDouble(AD_VALUE);
        AdvertCategory category = AdvertCategory.valueOf(resultSet.getString(AD_CATEGORY));
        TypeOfAnnouncement typeOfAnnouncement = TypeOfAnnouncement.valueOf(resultSet.getString(AD_TYPE));
        AnnouncementStatus announcementStatus = AnnouncementStatus.valueOf(resultSet.getString(AD_STATUS));
        return new Announcement(id, title, create_date, content, owner, contact,  value, category,
                typeOfAnnouncement, announcementStatus);
    }


    private PreparedStatement getStatement(String statement, Announcement announcement, Connection connection)
            throws SQLException {
        PreparedStatement ps  = connection.prepareStatement(statement);
        ps.setLong(1, announcement.getId());
        ps.setString(2, announcement.getTitle());
        ps.setString(3, announcement.getContent());
        ps.setLong(4, announcement.getAnnouncerId());
        ps.setString(5, new DecimalFormat("#.##").format(announcement.getValue()));
        ps.setString(6, announcement.getCategory().name());
        ps.setString(7, announcement.getTypeOfAnnouncement().name());
        ps.setString(8, announcement.getAnnouncementStatus().name());
        return ps;
    }

    private PreparedStatement getUpdateStatement(String statement, Announcement announcement, Connection connection)
            throws SQLException {
        PreparedStatement ps  = connection.prepareStatement(statement);
        ps.setString(1, announcement.getTitle());
        ps.setString(2, announcement.getContent());
        ps.setString(3, new DecimalFormat("#.##").format(announcement.getValue()));
        ps.setString(4, announcement.getCategory().name());
        ps.setString(5, announcement.getTypeOfAnnouncement().name());
        ps.setString(6, announcement.getAnnouncementStatus().name());
        ps.setLong(7, announcement.getId());
        return ps;
    }


}
