package com.epam.ans.app.model;

import com.epam.ans.app.entities.person.Moderator;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class ModeratorDAO {
    private final String MODERATOR_TABLE = "moderator";
    private final String MODERATOR_ID = "id";
    private final String MODERATOR_NAME = "user_name";
    private final String MODERATOR_PASSWORD = "password";
    private final String MODERATOR_PUBLIC_NAME = "public_name";

    private final DatabaseHandler dbHandler = DatabaseHandler.getInstance();

    //create moderator
    public boolean insertModerator(Moderator moderator) {
        boolean inserted = false;
        String statement = String.format("insert into %s (%s, %s, %s, %s) values (?, ?, ?, ?);",
                MODERATOR_TABLE, MODERATOR_ID, MODERATOR_NAME, MODERATOR_PASSWORD, MODERATOR_PUBLIC_NAME);
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement(statement)) {
            ps.setString(1, String.valueOf(moderator.getId()));
            ps.setString(2, moderator.getUserName());
            ps.setString(3, moderator.getPassword());
            ps.setString(3, moderator.getPublicName());
            inserted = ps.executeUpdate() > 0;
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return inserted;
    }

    //get moderator by id
    public Moderator getModeratorById(long id) {
        Moderator moderator = null;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select * from " +
                     MODERATOR_TABLE + " where " + MODERATOR_ID + " = '" + id + "';")) {

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString(MODERATOR_NAME);
                String password = resultSet.getString(MODERATOR_PASSWORD);
                String MODERATOR_CREATE_DATE = "create_date";
                Timestamp createDate = resultSet.getTimestamp(MODERATOR_CREATE_DATE);
                String publicName = resultSet.getString(MODERATOR_PUBLIC_NAME);
                moderator = new Moderator(id, name, password, createDate, publicName);
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return moderator;
    }
    //update moderator
    public boolean updateModerator(Moderator moderator) {
        boolean rowUpdated = false;
        String statement = String.format("update %s set %s = ?, %s = ? where %s = ?;",
                MODERATOR_TABLE, MODERATOR_NAME, MODERATOR_PUBLIC_NAME, MODERATOR_ID);
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement(statement)){
            ps.setString(1, moderator.getUserName());
            ps.setString(2, moderator.getPublicName());
            ps.setLong(3, moderator.getId());
            System.out.println("updateModerator" + ps);
            rowUpdated = ps.executeUpdate() > 0;
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }

        return rowUpdated;
    }

    public boolean updateModeratorPassword(long id, String newPassword) {
        boolean rowUpdated = false;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("update " + MODERATOR_TABLE + " set " +
                     MODERATOR_PASSWORD + " = ? where " + MODERATOR_ID + " = ?;")){
            ps.setString(1, newPassword);
            ps.setLong(2, id);
            System.out.println("updateModeratorPassword" + ps);
            rowUpdated = ps.executeUpdate() > 0;
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }

        return rowUpdated;
    }
}
