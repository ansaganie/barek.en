package com.epam.ans.app.model;

import com.epam.ans.app.entities.announcement.ContactUs;

import java.sql.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class ContactUsDAO {
    private final String CONTACT_US_TABLE = "contact_us";
    private final String CONTACT_US_ID = "id";
    private final String CONTACT_US_TITLE = "title";
    private final String CONTACT_US_CONTENT = "content";
    private final String CONTACT_US_OWNER_NAME = "owner_name";
    private final String CONTACT_US_OWNER_EMAIL = "owner_email";
    private final DatabaseHandler dbHandler = DatabaseHandler.getInstance();

    //create contact us record
    public boolean insertContactUs(ContactUs contactUs) {
        boolean inserted = false;

        String statement = String.format("insert into %s (%s, %s, %s, %s, %s) values (?, ?, ?, ?, ?);",
                CONTACT_US_TABLE, CONTACT_US_ID, CONTACT_US_TITLE, CONTACT_US_CONTENT, CONTACT_US_OWNER_NAME,
                CONTACT_US_OWNER_EMAIL);
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement(statement)) {
            ps.setLong(1, contactUs.getId());
            ps.setString(2, contactUs.getQuestionTitle());
            ps.setString(3, contactUs.getQuestionContent());
            ps.setString(4, contactUs.getOwnerName());
            ps.setString(5, contactUs.getOwnerEmail());
            inserted = ps.executeUpdate() > 0;
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return inserted;
    }

    //get record

    public Set<Long> getAllContactUsId() {
        Set<Long> ids = null;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select " + CONTACT_US_ID +
                     " from "+ CONTACT_US_TABLE +";")) {

            ResultSet resultSet = ps.executeQuery();
            ids = new HashSet<>();
            while (resultSet.next()) {
                ids.add(resultSet.getLong(CONTACT_US_ID));
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return ids;
    }

    public Long getContactUsCount () {
        return DAO.countRows(CONTACT_US_TABLE);
    }

    public Set<ContactUs> getQuestions() {
        Set<ContactUs> contactUsSet = new TreeSet<>(Comparator.comparing(ContactUs::getCreateDate));
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select * from " + CONTACT_US_TABLE + ";")) {
            ResultSet resultSet = ps.executeQuery();
            System.out.println("getQuestions " + ps);
            while (resultSet.next()) {
                Long id  = resultSet.getLong(CONTACT_US_ID);
                String title = resultSet.getString(CONTACT_US_TITLE);
                Timestamp createDate = resultSet.getTimestamp("create_date");
                String content = resultSet.getString(CONTACT_US_CONTENT);
                String ownerName = resultSet.getString(CONTACT_US_OWNER_NAME);
                String email = resultSet.getString(CONTACT_US_OWNER_EMAIL);
                ContactUs contactUs = new ContactUs(id, title,  content, createDate,ownerName, email);
                contactUsSet.add(contactUs);
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return contactUsSet;
    }
}
