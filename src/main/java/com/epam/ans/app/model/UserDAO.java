package com.epam.ans.app.model;

import com.epam.ans.app.entities.announcement.Announcement;
import com.epam.ans.app.entities.announcement.Contact;
import com.epam.ans.app.entities.person.Gender;
import com.epam.ans.app.entities.person.User;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Set;
import java.util.HashSet;


public class UserDAO {
    private final String USER_TABLE = "user";
    private final String USER_ID = "id";
    private final String USER_NAME = "user_name";
    private final String USER_PASSWORD = "password";
    private final String USER_GENDER = "gender";


    private final DatabaseHandler dbHandler = DatabaseHandler.getInstance();

    //create
    public boolean insertUser(User user) {
        boolean insertedUser = false;
        boolean insertedContact = false;
        String statement = String.format("insert into %s (%s, %s, %s, %s) values (?, ?, ?, ?);",
                USER_TABLE, USER_ID, USER_NAME, USER_PASSWORD, USER_GENDER);
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement(statement)) {
            ps.setString(1, String.valueOf(user.getId()));
            ps.setString(2, user.getUserName());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getGender().name());

            System.out.println(ps);
            insertedUser = ps.executeUpdate() > 0;
            insertedContact = DAO.getContactDAO().insertContact(user.getContact());
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return insertedContact && insertedUser;
    }



    //read
    public Set<Long> getAllUsersId() {
        Set<Long> ids = null;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select id from user;")) {

            ResultSet resultSet = ps.executeQuery();
            ids = new HashSet<>();
            while (resultSet.next()) {
                ids.add(resultSet.getLong(USER_ID));
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return ids;
    }

    public User getUserByUserName(String userName) {
        return getUser(USER_NAME, userName);
    }

    public User getUserById(long id) {
        return getUser(USER_ID, String.valueOf(id));
    }

    private User getUser(String parameterName, String parameter) {
        User user = null;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select * from " +
                     USER_TABLE + " where " + parameterName + " = '" + parameter + "';")) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                user = createUser(resultSet);
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return user;
    }

    public Set<User> getAllUsers() {
        Set<User> users = new HashSet<>();
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select * from " + USER_TABLE + ";")) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                User user = createUser(resultSet);
                users.add(user);
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }

        return users;
    }

    public Long getUsersCount() {
        return DAO.countRows(USER_TABLE);
    }


    //update
    public boolean updateUser(User user) {
        boolean userUpdated = false;
        boolean contactUpdated = false;
        String statement = String.format("update %s set %s = ?, %s = ? where %s = ?;",
                USER_TABLE, USER_NAME, USER_GENDER, USER_ID);
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement(statement)){
            ps.setString(1, user.getUserName());
            ps.setString(2, user.getGender().name());
            ps.setLong(3, user.getId());
            System.out.println(ps);

            contactUpdated = DAO.getContactDAO().updateContact(user.getContact());

            userUpdated = ps.executeUpdate() > 0;
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }

        return userUpdated && contactUpdated;
    }

    public boolean updateUserPassword(long id, String newPassword) {
        boolean rowUpdated = false;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("update " + USER_TABLE + " set " + USER_PASSWORD +
                     " = ? where " + USER_ID + " = ?;")){
            ps.setString(1, newPassword);
            ps.setLong(2, id);
            System.out.println(ps);
            rowUpdated = ps.executeUpdate() > 0;
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }

        return rowUpdated;
    }

    //delete
    public boolean deleteUser(long id) {
        return DAO.deleteRowById(USER_TABLE, USER_ID, id);
    }

    //helper methods

    private User createUser(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong(USER_ID);
        String name = resultSet.getString(USER_NAME);
        String password = resultSet.getString(USER_PASSWORD);
        String USER_TIMESTAMP = "create_date";
        Timestamp create_date = resultSet.getTimestamp(USER_TIMESTAMP);
        Gender gender = Gender.valueOf(resultSet.getString(USER_GENDER));
        Contact contact = DAO.getContactDAO().getContactByOwnerId(id);
        Set<Announcement> announcements = DAO.getAnnouncementDAO().getAnnouncementsByUserId(id);
        return new User(id, name, password, create_date, gender, announcements, contact);
    }
}
