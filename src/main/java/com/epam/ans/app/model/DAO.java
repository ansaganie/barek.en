package com.epam.ans.app.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAO {
    private static UserDAO userDAO;
    private static AnnouncementDAO announcementDAO;
    private static ContactDAO contactDAO;
    private static ContactUsDAO contactUsDAO;

    private static final DatabaseHandler dbHandler = DatabaseHandler.getInstance();

    public static UserDAO getUserDAO() {
        if (userDAO == null) userDAO = new UserDAO();
        return userDAO;
    }

    public static AnnouncementDAO getAnnouncementDAO() {
        if (announcementDAO == null) announcementDAO = new AnnouncementDAO();
        return announcementDAO;
    }

    public static ContactDAO getContactDAO() {
        if (contactDAO == null) contactDAO = new ContactDAO();
        return contactDAO;
    }

    public static ContactUsDAO getContactUsDAO() {
        if (contactUsDAO == null) contactUsDAO = new ContactUsDAO();
        return contactUsDAO;
    }

    public static boolean deleteRowById(String tableName, String rowName, long id) {
        boolean rowDeleted = false;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("delete from " + tableName +
                     " where " + rowName + " = ?;")){
            ps.setLong(1, id);
            System.out.println(ps);
            rowDeleted = ps.executeUpdate() > 0;
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }

        return rowDeleted;
    }

    public static Long countRows(String tableName) {
        Long count = null;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select count(*) as 'count-result' from " + tableName + ";")) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getLong("count-result");
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return count;
    }

    public static Long countRows(String tableName, String parameterName, String value) {
        Long count = null;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select count(*) as 'count-result' from " + tableName
                     + " where " + parameterName + " = ? ;")) {
            ps.setString(1, value);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getLong("count-result");
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return count;
    }
}
