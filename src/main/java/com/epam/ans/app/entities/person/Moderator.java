package com.epam.ans.app.entities.person;

import java.sql.Timestamp;

public class Moderator extends AbstractUser {
    private String publicName;

    public Moderator(Long id, String userName, String password, String publicName) {
        super(id, userName, password);
        this.publicName = publicName;
    }

    public Moderator(Long id, String userName, String password, Timestamp createDate, String publicName) {
        super(id, userName, password, createDate);
        this.publicName = publicName;
    }

    public String getPublicName() {
        return publicName;
    }

    public void setPublicName(String publicName) {
        this.publicName = publicName;
    }
}
