package com.epam.ans.app.entities.person;

import java.sql.Timestamp;

public abstract class AbstractUser extends Person {
    protected String password;

    public AbstractUser(Long id, String userName, String password) {
        super(id, userName);
        this.password = password;
    }

    public AbstractUser(Long id, String userName, String password, Timestamp createDate) {
        super(id, userName, createDate);
        this.password = password;

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
