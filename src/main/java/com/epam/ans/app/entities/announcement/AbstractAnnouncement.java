package com.epam.ans.app.entities.announcement;

import java.sql.Timestamp;
import java.util.Objects;

public abstract class AbstractAnnouncement {
    protected final Long id;
    protected String title;
    protected Timestamp createDate;
    protected String content;
    protected long announcerId;

    public AbstractAnnouncement(Long id, String title, Timestamp createDate, String content, long announcerId) {
        this.id = id;
        this.title = title;
        this.createDate = createDate;
        this.content = content;
        this.announcerId = announcerId;
    }

    public AbstractAnnouncement(Long id, String title, String content, long announcerId) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.announcerId = announcerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractAnnouncement)) return false;
        AbstractAnnouncement that = (AbstractAnnouncement) o;
        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getAnnouncerId() {
        return announcerId;
    }
}
