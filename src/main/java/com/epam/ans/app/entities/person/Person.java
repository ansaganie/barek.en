package com.epam.ans.app.entities.person;

import java.sql.Timestamp;
import java.util.Objects;

public abstract class Person {
    protected final Long id;
    protected String userName;
    protected Timestamp createDate;

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Person(Long id, String userName) {
        this.id = id;
        this.userName = userName;
    }

    public Person(Long id, String userName, Timestamp createDate) {
        this.id = id;
        this.userName = userName;
        this.createDate = createDate;
    }

    public Long getId() {
        return id;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return getId().equals(person.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
