package com.epam.ans.app.entities.announcement;

public class Contact {
    private final Long id;
    private String email;
    private String phoneNumber;
    private String location;
    private final Long ownerId;

    public Contact(Long id, String email, String phoneNumber, String location, Long ownerId) {
        this.id = id;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.location = location;
        this.ownerId = ownerId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
