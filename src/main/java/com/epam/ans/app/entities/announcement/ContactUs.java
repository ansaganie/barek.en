package com.epam.ans.app.entities.announcement;

import java.sql.Timestamp;
import java.util.Objects;

public class ContactUs {
    private final Long id;
    private final String questionTitle;
    private final String questionContent;
    private final String ownerName;
    private final String ownerEmail;
    private Timestamp createDate;

    public ContactUs(Long id, String title, String content, Timestamp createDate, String ownerName, String ownerEmail) {
        this.id = id;
        this.questionTitle = title;
        this.questionContent = content;
        this.ownerName = ownerName;
        this.ownerEmail = ownerEmail;
        this.createDate = createDate;
    }

    public ContactUs(Long id, String title, String content, String ownerName, String ownerEmail) {
        this.id = id;
        this.questionTitle = title;
        this.questionContent = content;
        this.ownerName = ownerName;
        this.ownerEmail = ownerEmail;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public Long getId() {
        return id;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContactUs)) return false;
        ContactUs contactUs = (ContactUs) o;
        return Objects.equals(getId(), contactUs.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
