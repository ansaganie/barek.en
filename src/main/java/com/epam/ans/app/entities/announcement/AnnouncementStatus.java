package com.epam.ans.app.entities.announcement;

public enum AnnouncementStatus {
    IN_REVIEW,
    ACTIVE,
    DEACTIVATED,
    REJECTED
}
