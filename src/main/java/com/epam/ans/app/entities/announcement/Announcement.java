package com.epam.ans.app.entities.announcement;

import java.io.Serializable;
import java.sql.Timestamp;

public class Announcement extends AbstractAnnouncement implements Serializable {
    private Contact contact;
    private double value;
    private AdvertCategory category;
    private TypeOfAnnouncement typeOfAnnouncement;
    private AnnouncementStatus announcementStatus;

    public Announcement(Long id, String title, Timestamp createDate, String content, long announcer,
                        Contact contact, double value, AdvertCategory category, TypeOfAnnouncement typeOfAnnouncement,
                        AnnouncementStatus announcementStatus) {
        super(id, title, createDate, content, announcer);
        this.contact = contact;
        this.value = value;
        this.category = category;
        this.typeOfAnnouncement = typeOfAnnouncement;
        this.announcementStatus = announcementStatus;
    }

    public Announcement(Long id, String title, String content, long announcer, Contact contact, double value,
                        AdvertCategory category, TypeOfAnnouncement typeOfAnnouncement,
                        AnnouncementStatus announcementStatus) {
        super(id, title, content, announcer);
        this.contact = contact;
        this.value = value;
        this.category = category;
        this.typeOfAnnouncement = typeOfAnnouncement;
        this.announcementStatus = announcementStatus;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public TypeOfAnnouncement getTypeOfAnnouncement() {
        return typeOfAnnouncement;
    }

    public void setTypeOfAnnouncement(TypeOfAnnouncement typeOfAnnouncement) {
        this.typeOfAnnouncement = typeOfAnnouncement;
    }

    public AnnouncementStatus getAnnouncementStatus() {
        return announcementStatus;
    }

    public void setAnnouncementStatus(AnnouncementStatus announcementStatus) {
        this.announcementStatus = announcementStatus;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public AdvertCategory getCategory() {
        return category;
    }

    public void setCategory(AdvertCategory category) {
        this.category = category;
    }
}
