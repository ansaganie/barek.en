package com.epam.ans.app.entities.person;

public enum Gender {
    M,
    F,
    NOT_DEFINED
}
