package com.epam.ans.app.entities.person;

import com.epam.ans.app.entities.announcement.Announcement;
import com.epam.ans.app.entities.announcement.Contact;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class User extends AbstractUser implements Serializable {
    private Gender gender;
    private Set<Announcement> announcements;
    private Contact contact;

    public User(Long id, String userName, String password, Gender gender, Set<Announcement> announcements, Contact contact) {
        super(id, userName, password);
        this.gender = gender;
        this.announcements = announcements;
        this.contact = contact;
    }

    public User(Long id, String userName, String password, Timestamp createDate, Gender gender, Set<Announcement> announcements, Contact contact) {
        super(id, userName, password, createDate);
        this.gender = gender;
        this.announcements = announcements;
        this.contact = contact;
    }

    public User(Long id, String name, String password, Gender gender, Contact contact) {
        super(id, name, password);
        this.gender = gender;
        this.contact = contact;
    }

    public void addAnnouncement(Announcement announcement) {
        this.announcements.add(announcement);
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Set<Announcement> getAnnouncements() {
        if (this.announcements == null)
            return new TreeSet<>(Comparator.comparing(Announcement::getCreateDate));
        return announcements;
    }

    public void setAnnouncements(Set<Announcement> announcements) {
        this.announcements = announcements;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
