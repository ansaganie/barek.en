package com.epam.ans.app.servelts.announcement;
import com.epam.ans.app.entities.announcement.Announcement;
import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;
import com.epam.ans.app.servelts.SessionUpdate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "delete-announcement", value = "/announcements/delete-announcement")
public class DeleteAnnouncement extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/announcements/delete-announcement.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User currentUser = (User) req.getSession().getAttribute("current-user");
        String answer = req.getParameter("answer-delete-announcement");
        Announcement announcement = (Announcement) req.getSession().getAttribute("announcement-to-modify");
        if (answer.equals("YES")) {
            boolean success = DAO.getAnnouncementDAO().deleteAnnouncementById(announcement.getId());
            if (success) {
                req.getSession().removeAttribute("announcement-to-modify");
                SessionUpdate.updateUserInSession(req.getSession(), DAO.getUserDAO().getUserById(currentUser.getId()));
                resp.sendRedirect("/user/profile");
            } else {
                throw new ServletException("announcement with id=" + announcement.getId() + " was not deleted");
            }
        } else {
            resp.sendRedirect("/user/profile");
        }
    }
}
