package com.epam.ans.app.servelts.announcement;

import com.epam.ans.app.entities.announcement.*;
import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;
import com.epam.ans.app.servelts.SessionUpdate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.rmi.ServerException;

@WebServlet(name = "update-announcement", value = "/announcements/update-announcement")
public class UpdateAnnouncement  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User currentUser = (User) req.getSession().getAttribute("current-user");
        String announcementID = req.getParameter("id");
        Announcement announcement;
        if (currentUser == null) resp.sendRedirect("/user/sign-in");
        else if (announcementID == null) resp.sendRedirect("/user/profile");
        else {
            try {
                announcement = DAO.getAnnouncementDAO().getAnnouncementById(Long.parseLong(announcementID));
            } catch (NumberFormatException e) {
                e.printStackTrace();
                throw new ServerException("There is no such announcement in DataBase");
            }
            if (announcement == null) {
                throw new ServerException("There is no such announcement in DataBase");
            } else if (!currentUser.getAnnouncements().contains(announcement)) {
                throw new ServletException("'UPDATE' of current announcement is restricted for '" +
                        currentUser.getUserName() + "'" );
            } else {
                req.setAttribute("announcement-update", announcement);
                req.getRequestDispatcher("/announcements/update-announcement.jsp").forward(req, resp);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User currentUser = (User) req.getSession().getAttribute("current-user");
        if (currentUser != null) {
            TypeOfAnnouncement typeOfAnnouncement = TypeOfAnnouncement.valueOf(req.getParameter("type-of-announcement"));
            AdvertCategory category = AdvertCategory.valueOf(req.getParameter("category"));
            String title = req.getParameter("announcement-title");
            String content = req.getParameter("announcement-content");
            double value = 0;
            String email = req.getParameter("email");
            String phoneNumber = req.getParameter("phone-number");
            String location = req.getParameter("location");
            long announcementId = 0;

            try {
                announcementId = Long.parseLong(req.getParameter("announcement-id"));
                value = Double.parseDouble(req.getParameter("value"));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            Announcement announcement = DAO.getAnnouncementDAO().getAnnouncementById(announcementId);
            announcement.setTitle(title);
            announcement.setContent(content);
            Contact contact = announcement.getContact();
            contact.setEmail(email);
            contact.setPhoneNumber(phoneNumber);
            contact.setLocation(location);
            announcement.setContact(contact);
            announcement.setValue(value);
            announcement.setCategory(category);
            announcement.setTypeOfAnnouncement(typeOfAnnouncement);
            announcement.setAnnouncementStatus(AnnouncementStatus.IN_REVIEW);
            boolean updated = DAO.getAnnouncementDAO().updateAnnouncement(announcement);
            currentUser.addAnnouncement(announcement);
            if (updated) {
                SessionUpdate.updateUserInSession(req.getSession(), DAO.getUserDAO().getUserById(currentUser.getId()));
                req.setAttribute("updated-announcement-id", announcement.getId());
                req.setAttribute("updated-announcement-title", announcement.getTitle());
            }
            doGet(req, resp);
        } else {
            resp.sendRedirect("/user/sign-in");
        }

    }
}
