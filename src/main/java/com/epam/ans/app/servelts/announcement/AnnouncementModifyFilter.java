package com.epam.ans.app.servelts.announcement;

import com.epam.ans.app.entities.announcement.Announcement;
import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;
import com.epam.ans.app.model.GlobalConstants;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AnnouncementModifyFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        User currentUser = (User) req.getSession().getAttribute(GlobalConstants.CURRENT_USER);
        String id = req.getParameter("id");
        if (currentUser == null) resp.sendRedirect("/user/sign-in");
        else if (id == null) resp.sendRedirect("/user/profile");
        else {
            try {
                Announcement announcement = DAO.getAnnouncementDAO().getAnnouncementById(Long.parseLong(id));
                if (announcement == null) {
                    throw new ServletException("There is no announcement with id=" + id);
                } else if (!currentUser.getAnnouncements().contains(announcement)) {
                    throw new ServletException("User with id=" + currentUser.getUserName() +
                            " can't modify announcement with id=" + id);
                } else {
                    req.getSession().setAttribute("announcement-to-modify", announcement);
                    filterChain.doFilter(req, resp);
                }
            } catch (NumberFormatException ignored) {
                throw new ServletException("There is no announcement with id=" + id);
            }
        }
    }
}
