package com.epam.ans.app.servelts.user;

import com.epam.ans.app.entities.announcement.Contact;
import com.epam.ans.app.entities.person.Gender;
import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;
import com.epam.ans.app.model.GlobalConstants;
import com.epam.ans.app.servelts.SessionUpdate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "update-profile", value = "/user/profile/update-profile")
public class UpdateProfile extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("current-user");
        if (user == null) {
            resp.sendRedirect("/user/sign-in");
        } else {
            req.setAttribute("user-update", user);
            req.getRequestDispatcher("update-profile.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User currentUser = (User) req.getSession().getAttribute(GlobalConstants.CURRENT_USER);
        String name = req.getParameter("user-name");
        String email = req.getParameter("email");
        String phoneNumber = req.getParameter(("phone-number"));
        String location = req.getParameter("location");
        Gender gender = Gender.valueOf(req.getParameter("gender"));
        if (currentUser != null) {
            currentUser.setUserName(name);
            Contact contact = currentUser.getContact();
            contact.setEmail(email);
            contact.setPhoneNumber(phoneNumber);
            contact.setLocation(location);
            currentUser.setGender(gender);

            boolean updateUser = DAO.getUserDAO().updateUser(currentUser);
            if (updateUser) {
                SessionUpdate.updateUserInSession(req.getSession(), DAO.getUserDAO().getUserById(currentUser.getId()));
                req.setAttribute("updated", Boolean.TRUE);
                doGet(req, resp);
            } else {
                throw new ServletException("User or contact was not updated");
            }
        } else {
            throw new ServletException("Only logged user can update profile");
        }
    }
}
