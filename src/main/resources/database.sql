CREATE DATABASE bareken_db;
USE bareken_db;
create table contact
(
    id bigint not null,
    email varchar(45) not null,
    phone_number varchar(45) not null,
    location varchar(45) not null,
    create_date timestamp default (CURRENT_TIMESTAMP) not null,
    owner_id bigint not null,
    constraint contact_owner_id_uindex
        unique (owner_id),
    constraint contact_uindex
        unique (id)
);

alter table contact
    add primary key (id);

create table contact_us
(
    id bigint not null,
    title varchar(100) not null,
    create_date timestamp default (CURRENT_TIMESTAMP) not null,
    content varchar(500) not null,
    owner_name varchar(45) default 'guest' not null,
    owner_email varchar(45) not null,
    constraint contact_us_id_uindex
        unique (id)
);

alter table contact_us
    add primary key (id);

create table moderator
(
    id bigint not null,
    user_name varchar(45) not null,
    password varchar(45) not null,
    create_date timestamp default (CURRENT_TIMESTAMP) not null,
    public_name varchar(45) not null,
    constraint moderator_id_uindex
        unique (id)
);

alter table moderator
    add primary key (id);

create table user
(
    id bigint not null,
    user_name varchar(45) not null,
    password varchar(45) null,
    create_date timestamp default (CURRENT_TIMESTAMP) not null,
    gender enum('M', 'F', 'NOT_DEFINED') null,
    constraint user_id_uindex
        unique (id),
    constraint user_user_name_uindex
        unique (user_name)
);

alter table user
    add primary key (id);

create table announcement
(
    id bigint not null,
    title varchar(100) not null,
    create_date timestamp default (CURRENT_TIMESTAMP) not null,
    content varchar(500) not null,
    owner bigint not null,
    value double not null,
    category enum('TRANSPORT', 'REALITY', 'ELECTRONICS', 'HOUSE_AND_GARDEN', 'JOB', 'CLOTHING', 'FOR_KIDS', 'SERVICES', 'ANIMALS', 'NOT_DEFINED') not null,
    type_of_advert enum('SELLING', 'BUYING', 'FOR_RENT', 'LOST', 'FOUND') not null,
    status enum('IN_REVIEW', 'ACTIVE', 'DEACTIVATED', 'REJECTED') not null,
    constraint id_UNIQUE
        unique (id),
    constraint fk_announcement_1
        foreign key (owner) references user (id)
            on update cascade on delete cascade
);

create index fk_announcement_1_idx
    on announcement (owner);

alter table announcement
    add primary key (id);

