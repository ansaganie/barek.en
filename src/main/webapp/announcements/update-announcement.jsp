<%@ page import="com.epam.ans.app.entities.announcement.Announcement" %>
<%@ page import="com.epam.ans.app.model.DAO" %>
<%@ page import="java.rmi.ServerException" %>
<%@ page import="com.epam.ans.app.entities.announcement.Contact" %>

<%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 21.02.21
  Time: 03:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Bareken: Update announcement </title>
    <%@include file="/header.jsp"%>

    <%
    String announcementID = request.getParameter("id");
    Long announcementId = (Long) request.getAttribute("updated-announcement-id");
    String advertTitle = (String) request.getAttribute("updated-announcement-title");
    //if advert was updated
    if (announcementId !=null && advertTitle != null) {%>
    <div class="container">
        <h1 class="h3 mb-3 font-weight-normal"> Your announcement
            <a href="/announcements/announcement?id=<%=announcementId%>">
                "<%=advertTitle%>"
            </a> has been updated and sent for review</h1>
    </div>
    <%request.removeAttribute("updated-announcement-id");
    request.removeAttribute("updated-announcement-title");
    }else {
        Announcement announcement = DAO.getAnnouncementDAO().getAnnouncementById(Long.parseLong(announcementID));
        String title = announcement.getTitle();
        String content = announcement.getContent();
        String ownerName = currentUser.getUserName();
        Contact contact = announcement.getContact();
        String email  = contact.getEmail();
        String phoneNumber = contact.getPhoneNumber();
        String location = contact.getLocation();
        double price = announcement.getValue();
        String category = announcement.getCategory().name();
        String typeOfAnnouncement = announcement.getTypeOfAnnouncement().name();
        request.setAttribute("typeOfAn", typeOfAnnouncement);
        request.setAttribute("category", category);%>
            <div class="container">
                <h1 class="h3 m-3 text-center font-weight-normal" id="page-title">SELLING ANNOUNCEMENT</h1>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <form method="post">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="type-of-announcement">Please choose the type of announcement that you are going to submit: </label>
                                </div>
                                <select class="custom-select" name="type-of-announcement" id="type-of-announcement" required>
                                    <option ${typeOfAn.equals("SELLING")?"selected":""} value="SELLING">Selling</option>
                                    <option ${typeOfAn.equals("BUYING")?"selected":""} value="BUYING">Buying</option>
                                    <option ${typeOfAn.equals("FOR_RENT")?"selected":""} value="FOR_RENT">For rent</option>
                                    <option ${typeOfAn.equals("LOST")?"selected":""} value="LOST">Lost</option>
                                    <option ${typeOfAn.equals("FOUND")?"selected":""} value="FOUND">Found</option>
                                </select>
                            </div>
                            <div class="input-group mb-3" id="categories">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="category">Advertisement category: </label>
                                </div>
                                <select class="custom-select" name="category" id="category" required>
                                    <option ${category.equals("NOT_DEFINED")?"selected":""} value="NOT_DEFINED">-- select --</option>
                                    <option ${category.equals("TRANSPORT")?"selected":""} value="TRANSPORT">Transport</option>
                                    <option ${category.equals("REALITY")?"selected":""} value="REALITY">Reality</option>
                                    <option ${category.equals("ELECTRONICS")?"selected":""} value="ELECTRONICS">Electronics</option>
                                    <option ${category.equals("HOUSE_AND_GARDEN")?"selected":""} value="HOUSE_AND_GARDEN">House and garden</option>
                                    <option ${category.equals("JOB")?"selected":""} value="JOB">Job</option>
                                    <option ${category.equals("CLOTHING")?"selected":""} value="CLOTHING">Clothing</option>
                                    <option ${category.equals("FOR_KIDS")?"selected":""} value="FOR_KIDS">For kids</option>
                                    <option ${category.equals("SERVICES")?"selected":""} value="SERVICES">Services</option>
                                    <option ${category.equals("ANIMALS")?"selected":""} value="ANIMALS">Animals</option>
                                </select>
                            </div>

                            <div class="row mb-3">
                                <label for="announcement-title" class="col-sm-2 col-form-label">Title:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="announcement-title" class="form-control" id="announcement-title" value="<%=title%>" required maxlength="100">
                                </div>
                            </div>


                            <div class="row mb-3">
                                <label for="announcement-content" class="col-sm-2 col-form-label">Content of announcement: </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="announcement-content" id="announcement-content" rows="5" required maxlength="500"><%=content%></textarea>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label" for="value" id="value-label"> Price:</label>
                                <div class="col-sm-1">
                                    <div class="input-group-text">KZT</div>
                                </div>
                                <div class="col-sm-2">
                                    <input type="number" name="value" class="form-control" id="value" value="<%=price%>" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="owner-name" class="col-sm-2 col-form-label">Your name: </label>
                                <div class="col-sm-10">
                                    <input type="text" name="owner-name" class="form-control" id="owner-name" value="<%=ownerName%>" disabled>
                                </div>
                            </div>
                            <div class="row mb-3 disabled">
                                <label for="email" class="col-sm-2 col-form-label">Contact email: </label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" class="form-control" id="email" value="<%=email%>" required maxlength="45">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="phone-number" class="col-sm-2 col-form-label">Contact phone number: </label>
                                <div class="col-sm-10">
                                    <input type="tel" name="phone-number" class="form-control" id="phone-number" value="<%=phoneNumber%>" required maxlength="45">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="location" class="col-sm-2 col-form-label">Location: </label>
                                <div class="col-sm-10">
                                    <input type="text" name="location" class="form-control" id="location" value="<%=location%>" required maxlength="45">
                                </div>
                            </div>
                            <div class="row disabled d-none">
                                <label for="announcement-id"></label>
                                <input type="text" name="announcement-id" class="form-control d-none" id="announcement-id" value="<%=announcementID%>">
                            </div>

                            <button type="submit" class="btn btn-primary">Update announcement</button>
                        </form>
                    </div>
                </div>
            </div>
        <%}%>
    <script>
        document.getElementById("new-ad-button").classList.add("active");
        let typeOfAnnouncement = document.getElementById("type-of-announcement");

        typeOfAnnouncement.addEventListener('change', (event) =>{
            let value = event.target.value;
            if (value === "LOST" || value === "FOUND") {
                document.getElementById("categories").classList.add("d-none");
                document.getElementById("value-label").textContent=("Award: ");
            } else {
                document.getElementById("categories").classList.remove("d-none");
                document.getElementById("value-label").textContent=("Price: ");
            }

            switch (value) {
                case "SELLING": document.getElementById("page-title").textContent=("SELLING ANNOUNCEMENT");
                    break;
                case "BUYING": document.getElementById("page-title").textContent=("BUYING ANNOUNCEMENT");
                    break;
                case "LOST": document.getElementById("page-title").textContent=("LOST ANNOUNCEMENT");
                    break;
                case "FOUND": document.getElementById("page-title").textContent=("FOUND ANNOUNCEMENT");
                    break;
                case "FOR_RENT": document.getElementById("page-title").textContent=("FOR RENT ANNOUNCEMENT");
                    break;
            }
        })

    </script>
    <%@include file="/footer.jsp"%>
