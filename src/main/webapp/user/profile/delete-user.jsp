<%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 20.02.21
  Time: 01:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>



<html>
<head>
        <%String userName = (String) request.getAttribute("user-name");%>
        <title>Bareken: Delete profile of <%=userName%> </title>
         <%@include file="/header.jsp"%>
        <div class="container">
            <form class="row g-3" method="post">
                <h1 class="h3 m-5 text-center font-weight-normal">Do you really want to delete your account?</h1>
                <div class="form-group row" role="group">
                    <div class="btn-group row col-sm-12">
                        <div class="col-md-5"></div>
                        <div class="col-md-1 form-check form-check-inline">
                            <input type="submit" class="btn-check" name="answer" id="yes" value="YES">
                            <label class="btn btn-outline-danger" for="yes">YES</label>
                        </div>
                        <div class="col-md-1 form-check form-check-inline">
                            <input type="submit" class="btn-check" name="answer" id="no" value="NO" checked>
                            <label class="btn btn-outline-success" for="no">NO</label>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </form>

        </div>
    <%@include file="/footer.jsp"%>
