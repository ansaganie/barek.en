<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Bareken: Sign in</title>
<%@include file="/header.jsp"%>
    <%if (currentUser != null) {
        response.sendRedirect("/user/profile");
    }%>

<div class="container m-5 mx-auto" style="width: 600px;">
    <form class="form-signin" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="user-name" class="sr-only">Username</label>
        <input name="user-name" type="text" id="user-name" class="form-control" placeholder="Enter your username" required autofocus="">
        <label for="password" class="sr-only">Password</label>
        <input name="password" type="password" id="password" class="form-control" placeholder="Enter your password" required>
        <%
            String unsuccessful = (String) request.getAttribute("unsuccessful");
        if (unsuccessful != null) {%>
        <div class="w3-panel w3-green w3-display-container w3-card-4 w3-round">
            <span onclick="this.parentElement.style.display='none'"
                  class="w3-button w3-margin-right w3-display-right w3-round-large w3-hover-green w3-border w3-border-green w3-hover-border-grey">x</span>
            <h3 class="h3 mb-3 font-weight-normal">email or password is incorrect, please, try again!</h3>
        </div>
        <%
            }
        %>
        <div class="btn-group m-3" role="group">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <a class="btn btn-lg btn-primary btn-block" href="/user/sign-up">Sign up</a>
        </div>

    </form>
</div>

    <script>
        const x = document.getElementById("sign-in-button");
        x.classList.add("d-none")
    </script>
<%@include file="/footer.jsp"%>
