<%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 17.01.21
  Time: 16:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Bareken: Adding new User</title>
<%@include file="/header.jsp"%>

<%
String user_name = (String) request.getAttribute("user-name");
String hide = null;

if (user_name != null) {%>
<div class="container">
    <h1 class="h3 m-3 font-weight-normal"> User "<%= user_name%>" successfully registered!</h1>
</div>
<%
hide = "d-none";
try {
Thread.sleep(1000);
} catch (InterruptedException e) {
    e.printStackTrace();
}
response.sendRedirect("/user/profile");
}
%>


<div class="container m-5 mx-auto <%=hide%>" style="width: 500px;">
    <form class="justify-content-xxl-start" method="post" >
        <h1 class="h3 mb-3 font-weight-normal">Please fill in</h1>
        <div class="form-group">
            <label class="container-fluid" for="user-name">Username (used to login)
                <input name="user-name" type="text" class="form-control" id="user-name" placeholder="Enter your username" required maxlength="45">
            </label>

            <label class="container-fluid" for="password">Password
                <input name="password" class="form-control" type="password" id="password" placeholder="Enter password" required maxlength="45">
            </label>

            <label class="container-fluid" for="email">Email
                <input name="email" class="form-control" type="email" id="email" placeholder="Enter your email" required maxlength="45">
            </label>

            <label class="container-fluid" for="phone-number">Phone number
                <input name="phone-number" class="form-control" type="text" id="phone-number" placeholder="Enter phone number" required maxlength="45">
            </label>

            <label class="container-fluid" for="location">Location
                <input name="location" class="form-control" type="text" id="location" placeholder="Enter your location" required maxlength="45">
            </label>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="gender" id="male" value="M" required>
                <label class="form-check-label" for="male">Male</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="gender" id="female" value="F" required>
                <label class="form-check-label" for="female">Female</label>
            </div>
        </div>
        <input class="btn btn-primary" type="submit" value="Submit">
    </form>
</div>
<%@include file="/footer.jsp"%>