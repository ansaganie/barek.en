<%@ page import="com.epam.ans.app.entities.person.User" %>
<%String parameterType = request.getParameter("type");
request.setAttribute("type", parameterType);%>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="${pageContext.request.contextPath}/">Bareken</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a id="selling-button" class="nav-link ${type.equals("SELLING")?"active":""}" href="/announcements?type=SELLING">Selling</a>
                </li>
                <li class="nav-item">
                    <a id="buying-button" class="nav-link ${type.equals("BUYING")?"active":""}" href="/announcements?type=BUYING">Buying</a>
                </li>
                <li class="nav-item">
                    <a id="for-rent-button" class="nav-link ${type.equals("FOR_RENT")?"active":""}" href="/announcements?type=FOR_RENT">For rent</a>
                </li>
                <li class="nav-item">
                    <a id="lost-button" class="nav-link ${type.equals("LOST")?"active":""}" href="/announcements?type=LOST">Lost</a>
                </li>
                <li class="nav-item">
                    <a id="found-button" class="nav-link ${type.equals("FOUND")?"active":""}" href="/announcements?type=FOUND">Found</a>
                </li>
                <li class="nav-item">
                    <a id="new-ad-button"  class="nav-link" href="/announcements/add-announcement">Submit your Announcement</a>
                </li>
                <li class="nav-item">
                    <a id="contact-us-ad-button"  class="nav-link" href="/contact-us">Contact us</a>
                </li>
            </ul>
            <ul class="navbar-nav justify-content-end" id="sign-in-button">
            <li class="nav-item">
            <%User currentUser = (User) session.getAttribute("current-user");

            if (currentUser != null) {
                String emailSession = currentUser.getUserName();%>
                    <a id="" href="/user/profile" class="btn btn-primary"><%=emailSession%></a>
                </li>
            </ul>
            <%} else {%>
                    <a href="/user/sign-in" class="btn btn-primary">Sign in</a>
                </li>
            </ul>
            <%}%>
        </div>
    </div>
</nav>
