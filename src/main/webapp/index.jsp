<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Bareken: Selling, Buying and LostFound Announcements</title>
<%@include file="header.jsp"%>
    <div class="container">
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="images/1.png" class="d-block w-100" alt="Posters">
                </div>
                <div class="carousel-item">
                    <img src="images/2.png" class="d-block w-100" alt="Posters">
                </div>
            </div>
        </div>

    </div>

<%@include file="footer.jsp"%>