<%@ page import="com.epam.ans.app.model.AnnouncementDAO" %>
<%@ page import="com.epam.ans.app.model.DAO" %>
<%@ page import="com.epam.ans.app.entities.announcement.Announcement" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.epam.ans.app.entities.announcement.AnnouncementStatus" %><%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 03.03.21
  Time: 16:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%
    AnnouncementDAO announcementDAO = DAO.getAnnouncementDAO();
    Set<Announcement> announcements = announcementDAO.getAnnouncements(AnnouncementStatus.IN_REVIEW);
%>
<html>
<head>
    <title>Admin: Announcements</title>
    <%@include file="../header.jsp" %>

    <div class="container m-5">
        <%if (announcements.size() == 0) {%>
        <div class="w3-panel w3-green w3-display-container w3-card-4 w3-round">
                    <span onclick="this.parentElement.style.display='none'" class="w3-button w3-margin-right
                    w3-display-right w3-round-large w3-hover-green w3-border w3-border-green w3-hover-border-grey">x</span>
            <h3 class="h3 m-5 font-weight-normal text-center">
                Sorry, currently there is still no announcements in review</h3>
        </div>
        <%} else {
            for (Announcement ann : announcements) {
                long id = ann.getId();
                String title = ann.getTitle();
                String create_date = ann.getCreateDate().toString();
                double price = ann.getValue();
                String typeOfAnnouncement = ann.getTypeOfAnnouncement().name();%>
        <div class="card text-dark bg-light mb-3" style="max-width: 35rem">
            <div class="card-header"><a href="${pageContext.request.contextPath}announcements/announcement?id=<%=id%>"><%=title%></a></div>
            <div class="card-body">
                <h5 class="card-title"><%=title%></h5>
                <p class="card-text"><%=create_date%></p>
                <h5 class="card-title">Price</h5>
                <p class="card-text"><%=price%></p>
                <%if (!typeOfAnnouncement.equals("LOST") && !typeOfAnnouncement.equals("FOUND")) {
                    String category = ann.getCategory().name();%>
                <h5 class="card-title">Category</h5>
                <p class="card-text"> <%=category%></p>
                <%}%>
            </div>
        </div>
        <%}
        }%>
    </div>

    <%@include file="../footer.jsp"%>