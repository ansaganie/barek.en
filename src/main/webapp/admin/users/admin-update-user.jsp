<%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 03.03.21
  Time: 18:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.epam.ans.app.entities.person.User" %>
<%@ page import="com.epam.ans.app.entities.announcement.Contact" %>
<%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 17.01.21
  Time: 20:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%
    User user = (User) request.getAttribute("user-update");
    long id = user.getId();
    String userName = user.getUserName();
    String gender = user.getGender().name();
    request.setAttribute("gender", gender);
    Contact contact = user.getContact();
    String email = contact.getEmail();
    String phoneNumber = contact.getPhoneNumber();
    String location = contact.getLocation();
%>

<!DOCTYPE html>
<html>
<head>
    <title>Admin: Updating <%=userName%>'s profile</title>
    <%@include file="../header.jsp"%>

        <%
            Boolean result = (Boolean) request.getAttribute("updated");
        %>

        <%if (result != null && result.equals(Boolean.TRUE)) {%>
            <div class="container">
                <h1 class="h3 m-5 font-weight-normal text-center"> User's profile was successfully updated!</h1>
            </div>
        <%}%>

        <div class="container m-5 mx-auto" style="width: 500px;">
            <form class="justify-content-xxl-start" method="post" >
                <h1 class="h3 m-3 font-weight-normal">Please fill in</h1>
                <div class="form-group">
                    <label class="container-fluid" for="user-name">User's name
                        <input name="user-name" type="text" class="form-control" id="user-name" value="<%=userName%>">
                    </label>

                    <label class="container-fluid d-none" for="user-id">User's id
                        <input name="user-id" type="text" class="form-control" id="user-id" value="<%=id%>">
                    </label>

                    <label class="container-fluid" for="email">User's email
                        <input name="email" class="form-control" type="email" id="email" value="<%=email%>">
                    </label>

                    <label class="container-fluid" for="phone-number">User's phone number
                        <input name="phone-number" class="form-control" type="text" id="phone-number" value="<%=phoneNumber%>">
                    </label>

                    <label class="container-fluid" for="phone-number">User's location
                        <input name="location" class="form-control" type="text" id="location" value="<%=location%>">
                    </label>
                    <div class="container m-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gender" id="male" value="M" ${gender.equals("M") ? "checked":""}>
                            <label class="form-check-label" for="male">Male</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gender" id="female" value="F" ${gender.equals("F") ? "checked":""}>
                            <label class="form-check-label" for="female">Female</label>
                        </div>
                    </div>
                </div>
                <input class="btn btn-primary m-3" type="submit" value="Submit">
            </form>
        </div>
    <%@include file="../footer.jsp"%>

