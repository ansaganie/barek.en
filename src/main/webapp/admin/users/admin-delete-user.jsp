<%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 03.03.21
  Time: 18:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%
    String userId = request.getParameter("id");
%>
<html>
<head>
    <title>Amin: Delete user account</title>
    <%@include file="../header.jsp"%>
    <div class="container">
        <form class="row g-3" method="post">
            <h1 class="h3 m-5 text-center font-weight-normal">Do you really want to this account?</h1>
            <div class="form-group row" role="group">
                <div class="btn-group row col-sm-12">
                    <div class="col-md-5"></div>
                    <div class="col-md-1 form-check form-check-inline">
                        <input type="submit" class="btn-check" name="delete-user-answer" id="yes" value="YES">
                        <label class="btn btn-outline-danger" for="yes">YES</label>
                    </div>
                    <div class="col-md-1 form-check form-check-inline">
                        <input type="submit" class="btn-check" name="delete-user-answer" id="no" value="NO" checked>
                        <label class="btn btn-outline-success" for="no">NO</label>
                    </div>
                    <label class="d-none">
                        <input type="text" class="d-none" name="user-id" value="<%=userId%>">
                    </label>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </form>
    </div>
    <%@include file="../footer.jsp"%>
